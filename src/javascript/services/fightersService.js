import { callApi } from '../helpers/apiHelper';


class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    try {
      const endpoints = `details/fighter/${id}.json`;
      const apiResults = await callApi(endpoints, 'GET');

      return apiResults;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
