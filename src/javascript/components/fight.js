import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    // console.log(1);
    // fightProcess();
    // runOnKeys(
    //   () => console.log(getDamage(firstFighter, secondFighter)),
    //   controls.PlayerOneAttack
    // );
    // runOnKeys(
    //   () => console.log(getDamage(secondFighter, firstFighter)),
    //   controls.PlayerTwoAttack
    // );
    runOnKeys(
      () => console.log(getDamage(firstFighter, secondFighter, true)),
      controls.PlayerOneAttack,
      controls.PlayerTwoBlock
    );
    runOnKeys(
      () => console.log(getDamage(secondFighter, firstFighter, true)),
      controls.PlayerTwoAttack,
      controls.PlayerOneBlock
    );
    runOnKeys(
      () => console.log(getDamage(firstFighter, secondFighter)),
      controls.PlayerOneAttack
    );
    runOnKeys(
      () => console.log(getDamage(secondFighter, firstFighter)),
      controls.PlayerTwoAttack
    );
  });
}

export function getDamage(attacker, defender, block = false) {
  if(block == false) {return getHitPower(attacker);}
  else {
    if(getHitPower(attacker) - getBlockPower(defender) < 0) {return 0;}
    return getHitPower(attacker) - getBlockPower(defender);
  }
  // return damage
}

export function getHitPower(fighter) {
  const attack = fighter.get('attack');
  const criticalHitChance = (Math.random() + 1);
  const power = attack * criticalHitChance;
  return power;
  // return hit power
}

export function getBlockPower(fighter) {
  const defense = fighter.get('defense');
  const dodgeChance = (Math.random() + 1);
  const power = defense * dodgeChance;
  return power;
  // return block power
}

// function fightProcess(leftFighter, rightFighter) {
//   window.addEventListener('keydown', (e) => {
//     e.preventDefault();
//     if(e.code == controls.PlayerOneAttack && e.code == controls.PlayerTwoAttack){console.log('e.code');}
//     // console.log(e.code);
//     let lastPressedKeyCode = null;
//     // if (e.code == controls.PlayerOneAttack) {
//     //   console.log('p1attack');
//     // }
//     // switch(e.code) {
//     //   case controls.PlayerOneAttack : console.log('p1attack');
//     //   if (lastPressedKeyCode == controls.PlayerTwoBlock) {}
//     //    lastPressedKeyCode = controls.PlayerOneAttack;
//     //     break;
//     //   case controls.PlayerTwoAttack : console.log('p2attack');  lastPressedKeyCode = controls.PlayerTwoAttack; break;
//     //   case controls.PlayerOneBlock : console.log('p1block');  lastPressedKeyCode = controls.PlayerOneBlock; break;
//     //   case controls.PlayerTwoBlock : console.log('p2block');  lastPressedKeyCode = controls.PlayerOneBlock; break;
//     // }
//   });
// }

function runOnKeys(funct, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', function(event) {
    pressed.add(event.code);

    for (let code of codes) { 
      if (!pressed.has(code)) {
        return;
      }
    }


    funct();
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code);
  });

}
