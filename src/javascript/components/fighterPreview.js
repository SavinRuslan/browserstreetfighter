import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
   console.log(fighter.get('name'));
//
  fighterElement.innerHTML = 
  `
  <p>${fighter.get('name')}</p>
  <p>Health:${fighter.get('health')}</p>
  <p>Attack:${fighter.get('attack')}</p>
  <p>Defense:${fighter.get('defense')}</p>
  `;
  console.log(fighterElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  //const { source, name } = fighter;
  const attributes = { 
    src: fighter.get('source'), 
    title: fighter.get('name'),
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
